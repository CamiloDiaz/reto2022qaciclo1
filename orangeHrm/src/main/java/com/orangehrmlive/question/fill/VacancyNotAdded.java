package com.orangehrmlive.question.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.vacancypage.VacancyPage.NO_HIRING_MANAGER_NAME_VALIDATION;

public class VacancyNotAdded implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return NO_HIRING_MANAGER_NAME_VALIDATION.resolveFor(actor).isVisible();
    }

    public VacancyNotAdded is(){
        return this;
    }

    public static VacancyNotAdded vacancyNotAddedWithVisibleMessageInvalidHiringManagerField(){
        return new VacancyNotAdded();
    }
}
