package com.orangehrmlive.question.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static com.orangehrmlive.userinterface.qualificationpage.QualificationPage.EDUCATION_YEAR_VALIDATION;

public class EducationInformationAdded implements Question<Boolean> {

    private String educationValidationMessage;

    public EducationInformationAdded wasValidatedWithYearEducation(String educationValidationMessage) {
        this.educationValidationMessage = educationValidationMessage;
        return this;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return EDUCATION_YEAR_VALIDATION.resolveFor(actor).containsText(educationValidationMessage);
    }

    public EducationInformationAdded is(){
        return this;
    }

    public static EducationInformationAdded educationInformationAdded(){
        return new EducationInformationAdded();
    }
}
