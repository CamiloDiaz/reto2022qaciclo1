package com.orangehrmlive.task.fill;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.conditions.Check;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static com.orangehrmlive.userinterface.qualificationpage.QualificationPage.*;
import static com.orangehrmlive.util.EnumTimeOut.TWENTY_TWO;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class FillQualificationsWorkAndEducation implements Task {

    private String companyName;
    private String jobAtCompany;
    private String monthExperienceFrom;
    private String yearExperienceFrom;
    private String monthExperienceTo;
    private String yearExperienceTo;
    private String experienceComment;

    private String educationLevel;
    private String institute;
    private String specialization;
    private String yearEducation;
    private String educationScore;
    private String monthEducationStart;
    private String yearEducationStart;
    private String monthEducationEnd;
    private String yearEducationEnd;

    public FillQualificationsWorkAndEducation typingTheCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public FillQualificationsWorkAndEducation typingTheJobAtCompany(String jobAtCompany) {
        this.jobAtCompany = jobAtCompany;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheMonthExperienceFrom(String monthExperienceFrom) {
        this.monthExperienceFrom = monthExperienceFrom;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheYearExperienceFrom(String yearExperienceFrom) {
        this.yearExperienceFrom = yearExperienceFrom;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheMonthExperienceTo(String monthExperienceTo) {
        this.monthExperienceTo = monthExperienceTo;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheYearExperienceTo(String yearExperienceTo) {
        this.yearExperienceTo = yearExperienceTo;
        return this;
    }

    public FillQualificationsWorkAndEducation typingTheExperienceComment(String experienceComment) {
        this.experienceComment = experienceComment;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
        return this;
    }

    public FillQualificationsWorkAndEducation typingTheInstituteName(String institute) {
        this.institute = institute;
        return this;
    }

    public FillQualificationsWorkAndEducation typingTheSpecializationName(String specialization) {
        this.specialization = specialization;
        return this;
    }

    public FillQualificationsWorkAndEducation typingTheYearEducation(String yearEducation) {
        this.yearEducation = yearEducation;
        return this;
    }

    public FillQualificationsWorkAndEducation typingTheEducationScore(String educationScore) {
        this.educationScore = educationScore;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheMonthEducationStart(String monthEducationStart) {
        this.monthEducationStart = monthEducationStart;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheYearEducationStart(String yearEducationStart) {
        this.yearEducationStart = yearEducationStart;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheMonthEducationEnd(String monthEducationEnd) {
        this.monthEducationEnd = monthEducationEnd;
        return this;
    }

    public FillQualificationsWorkAndEducation selectingTheYearEducationEnd(String yearEducationEnd) {
        this.yearEducationEnd = yearEducationEnd;
        return this;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                WaitUntil.the   (ADD_WORK_EXPERIENCE_BTN,isVisible()).forNoMoreThan(TWENTY_TWO.getValue()).seconds(),
                Check.whether(DELETE_WORK_EXPERIENCE_BTN.resolveFor(actor).isVisible())
                        .andIfSo(
                                Scroll.to(SELECT_ALL_WORKS),
                                Click.on(SELECT_ALL_WORKS),

                                Scroll.to(DELETE_WORK_EXPERIENCE_BTN),
                                Click.on(DELETE_WORK_EXPERIENCE_BTN)
                        ),

                Scroll.to(ADD_WORK_EXPERIENCE_BTN),
                Click.on(ADD_WORK_EXPERIENCE_BTN),

                Scroll.to(EXPERIENCE_COMPANY),
                Enter.theValue  (companyName).into(EXPERIENCE_COMPANY),

                Scroll.to(EXPERIENCE_JOB_TITLE),
                Enter.theValue  (jobAtCompany).into(EXPERIENCE_JOB_TITLE),

                Scroll.to(EXPERIENCE_FROM_DATE),
                Click.on        (EXPERIENCE_FROM_DATE),

                Scroll.to(MONTH_DATE),
                SelectFromOptions.byValue(monthExperienceFrom).from(MONTH_DATE),

                Scroll.to(YEAR_DATE),
                SelectFromOptions.byValue(yearExperienceFrom).from(YEAR_DATE),

                Scroll.to(DAY_DATE),
                Click.on        (DAY_DATE),

                Scroll.to(EXPERIENCE_TO_DATE),
                Click.on        (EXPERIENCE_TO_DATE),

                Scroll.to(MONTH_DATE),
                SelectFromOptions.byValue(monthExperienceTo).from(MONTH_DATE),

                Scroll.to(YEAR_DATE),
                SelectFromOptions.byValue(yearExperienceTo).from(YEAR_DATE),

                Scroll.to(DAY_DATE),
                Click.on        (DAY_DATE),

                Scroll.to(EXPERIENCE_COMMENT),
                Enter.theValue  (experienceComment).into(EXPERIENCE_COMMENT),

                Scroll.to(WORK_EXPERIENCE_SAVE_BTN),
                Click.on        (WORK_EXPERIENCE_SAVE_BTN),

                Check.whether(DELETE_EDUCATION_BTN.resolveFor(actor).isVisible())
                        .andIfSo(
                                Scroll.to(SELECT_ALL_EDUCATIONS),
                                Click.on(SELECT_ALL_EDUCATIONS),

                                Scroll.to(DELETE_EDUCATION_BTN),
                                Click.on(DELETE_EDUCATION_BTN)
                        ),

                Scroll.to(ADD_EDUCATION_BTN),
                Click.on(ADD_EDUCATION_BTN),

                Scroll.to(SELECT_EDUCATION_LEVEL),
                SelectFromOptions.byValue(educationLevel).from(SELECT_EDUCATION_LEVEL),

                Scroll.to(INSTITUTE),
                Enter.theValue  (institute).into(INSTITUTE),

                Scroll.to(MAJOR_SPECIALIZATION),
                Enter.theValue  (specialization).into(MAJOR_SPECIALIZATION),

                Scroll.to(EDUCATION_YEAR),
                Enter.theValue  (yearEducation).into(EDUCATION_YEAR),

                Scroll.to(GPA_SCORE),
                Enter.theValue  (educationScore).into(GPA_SCORE),

                Scroll.to(EDUCATION_START_DATE),
                Click.on        (EDUCATION_START_DATE),

                Scroll.to(MONTH_DATE),
                SelectFromOptions.byValue(monthEducationStart).from(MONTH_DATE),

                Scroll.to(YEAR_DATE),
                SelectFromOptions.byValue(yearEducationStart).from(YEAR_DATE),

                Scroll.to(DAY_DATE),
                Click.on        (DAY_DATE),

                Scroll.to(EDUCATION_END_DATE),
                Click.on        (EDUCATION_END_DATE),

                Scroll.to(MONTH_DATE),
                SelectFromOptions.byValue(monthEducationEnd).from(MONTH_DATE),

                Scroll.to(YEAR_DATE),
                SelectFromOptions.byValue(yearEducationEnd).from(YEAR_DATE),

                Scroll.to(DAY_DATE),
                Click.on        (DAY_DATE),

                Scroll.to(EDUCATION_SAVE_BTN),
                Click.on        (EDUCATION_SAVE_BTN)

        );

    }

    public static FillQualificationsWorkAndEducation fillQualificationsWorkAndEducation(){
        return new FillQualificationsWorkAndEducation();
    }

}
