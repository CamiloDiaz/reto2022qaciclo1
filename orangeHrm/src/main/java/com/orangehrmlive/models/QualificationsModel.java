package com.orangehrmlive.models;

public class QualificationsModel {

    private String companyNameModel;
    private String jobAtCompanyModel;
    private String monthExperienceFromModel;
    private String yearExperienceFromModel;
    private String monthExperienceToModel;
    private String yearExperienceToModel;
    private String experienceCommentModel;

    private String educationLevelModel;
    private String instituteModel;
    private String specializationModel;
    private String yearEducationModel;
    private String educationScoreModel;
    private String monthEducationStartModel;
    private String yearEducationStartModel;
    private String monthEducationEndModel;
    private String yearEducationEndModel;

    private String skillModel;
    private String yearsOfExperienceModel;
    private String skillCommentModel;

    private String languageModel;
    private String languageFluencyModel;
    private String languageCompetencyModel;
    private String languageCommentModel;

    public String getCompanyNameModel() {
        return companyNameModel;
    }

    public void setCompanyNameModel(String companyNameModel) {
        this.companyNameModel = companyNameModel;
    }

    public String getJobAtCompanyModel() {
        return jobAtCompanyModel;
    }

    public void setJobAtCompanyModel(String jobAtCompanyModel) {
        this.jobAtCompanyModel = jobAtCompanyModel;
    }

    public String getMonthExperienceFromModel() {
        return monthExperienceFromModel;
    }

    public void setMonthExperienceFromModel(String monthExperienceFromModel) {
        this.monthExperienceFromModel = monthExperienceFromModel;
    }

    public String getYearExperienceFromModel() {
        return yearExperienceFromModel;
    }

    public void setYearExperienceFromModel(String yearExperienceFromModel) {
        this.yearExperienceFromModel = yearExperienceFromModel;
    }

    public String getMonthExperienceToModel() {
        return monthExperienceToModel;
    }

    public void setMonthExperienceToModel(String monthExperienceToModel) {
        this.monthExperienceToModel = monthExperienceToModel;
    }

    public String getYearExperienceToModel() {
        return yearExperienceToModel;
    }

    public void setYearExperienceToModel(String yearExperienceToModel) {
        this.yearExperienceToModel = yearExperienceToModel;
    }

    public String getExperienceCommentModel() {
        return experienceCommentModel;
    }

    public void setExperienceCommentModel(String experienceCommentModel) {
        this.experienceCommentModel = experienceCommentModel;
    }

    public String getEducationLevelModel() {
        return educationLevelModel;
    }

    public void setEducationLevelModel(String educationLevelModel) {
        this.educationLevelModel = educationLevelModel;
    }

    public String getInstituteModel() {
        return instituteModel;
    }

    public void setInstituteModel(String instituteModel) {
        this.instituteModel = instituteModel;
    }

    public String getSpecializationModel() {
        return specializationModel;
    }

    public void setSpecializationModel(String specializationModel) {
        this.specializationModel = specializationModel;
    }

    public String getYearEducationModel() {
        return yearEducationModel;
    }

    public void setYearEducationModel(String yearEducationModel) {
        this.yearEducationModel = yearEducationModel;
    }

    public String getEducationScoreModel() {
        return educationScoreModel;
    }

    public void setEducationScoreModel(String educationScoreModel) {
        this.educationScoreModel = educationScoreModel;
    }

    public String getMonthEducationStartModel() {
        return monthEducationStartModel;
    }

    public void setMonthEducationStartModel(String monthEducationStartModel) {
        this.monthEducationStartModel = monthEducationStartModel;
    }

    public String getYearEducationStartModel() {
        return yearEducationStartModel;
    }

    public void setYearEducationStartModel(String yearEducationStartModel) {
        this.yearEducationStartModel = yearEducationStartModel;
    }

    public String getMonthEducationEndModel() {
        return monthEducationEndModel;
    }

    public void setMonthEducationEndModel(String monthEducationEndModel) {
        this.monthEducationEndModel = monthEducationEndModel;
    }

    public String getYearEducationEndModel() {
        return yearEducationEndModel;
    }

    public void setYearEducationEndModel(String yearEducationEndModel) {
        this.yearEducationEndModel = yearEducationEndModel;
    }

    public String getSkillModel() {
        return skillModel;
    }

    public void setSkillModel(String skillModel) {
        this.skillModel = skillModel;
    }

    public String getYearsOfExperienceModel() {
        return yearsOfExperienceModel;
    }

    public void setYearsOfExperienceModel(String yearsOfExperienceModel) {
        this.yearsOfExperienceModel = yearsOfExperienceModel;
    }

    public String getSkillCommentModel() {
        return skillCommentModel;
    }

    public void setSkillCommentModel(String skillCommentModel) {
        this.skillCommentModel = skillCommentModel;
    }

    public String getLanguageModel() {
        return languageModel;
    }

    public void setLanguageModel(String languageModel) {
        this.languageModel = languageModel;
    }

    public String getLanguageFluencyModel() {
        return languageFluencyModel;
    }

    public void setLanguageFluencyModel(String languageFluencyModel) {
        this.languageFluencyModel = languageFluencyModel;
    }

    public String getLanguageCompetencyModel() {
        return languageCompetencyModel;
    }

    public void setLanguageCompetencyModel(String languageCompetencyModel) {
        this.languageCompetencyModel = languageCompetencyModel;
    }

    public String getLanguageCommentModel() {
        return languageCommentModel;
    }

    public void setLanguageCommentModel(String languageCommentModel) {
        this.languageCommentModel = languageCommentModel;
    }
}
