package com.orangehrmlive.userinterface.myinfointerface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MyInfoInterface extends PageObject {

    public static final Target BTN_MY_INFO = Target
            .the("Btn my info")
            .located(By.id("menu_pim_viewMyDetails"));

    public static final Target BTN = Target
            .the("button")
            .located(By.id("btnSave"));

    public static final Target FIRST_NAME_DETAILS = Target
            .the("First Name Details txt")
            .located(By.id("personal_txtEmpFirstName"));



}
