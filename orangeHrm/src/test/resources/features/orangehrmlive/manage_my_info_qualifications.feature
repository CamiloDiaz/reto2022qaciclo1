Feature: Manage My Info Qualifications
  As the administrator
  I want to be able to manage the profile info qualifications Orange Web Page
  To have better control of my profile info in the company

  Scenario: Reset and Add new information
    Given I enter to the login Page of Orange Web Page
    When  I login and enter to the Qualifications section
    And   I add Work Experience and Education information and save it
    And   I add Skill and Language information and save it
    Then  All saved information should appear in the Qualifications Section