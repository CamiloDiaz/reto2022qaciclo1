package com.orangehrmlive.stepdefinitions;

import com.orangehrmlive.stepdefinitions.setup.SetUp;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.log4j.Logger;

import static com.orangehrmlive.question.login.UserLoggedIn.userLoggedInValidatedWithVisibleWelcomeMessage;
import static com.orangehrmlive.question.login.UserNotLoggedIn.userNotLoggedInValidatedWithVisibleMessage;
import static com.orangehrmlive.task.fill.FillLogIn.fillLogIn;
import static com.orangehrmlive.util.Dictionary.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.CoreMatchers.equalTo;

public class LogInStepDefinition extends SetUp {

    private static final Logger LOGGER = Logger.getLogger(LogInStepDefinition.class);

    @When("I enter my user information")
    public void iEnterMyUserInformation() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    fillLogIn()
                            .withUserName(USER)
                            .andPassword(PASS)
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
    }

    @Then("I should see my information account")
    public void iShouldSeeMyInformationAccount() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            userLoggedInValidatedWithVisibleWelcomeMessage()
                                    .is(), equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

    @When("I only enter my user name")
    public void iOnlyEnterMyUserName() {

        try{
            theActorInTheSpotlight().attemptsTo(
                    fillLogIn()
                            .withUserName(USER)
                            .andPassword(EMPTY_STRING)
            );

        }catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }

    }

    @Then("I should see an error account validation")
    public void iShouldSeeAnErrorAccountValidation() {

        try {
            theActorInTheSpotlight().should(
                    seeThat(
                            userNotLoggedInValidatedWithVisibleMessage()
                                    .is(), equalTo(true)
                    )
            );
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
    }

}
