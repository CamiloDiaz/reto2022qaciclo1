package com.orangehrmlive.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        strict = true,
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/orangehrmlive/manage_my_info_qualifications.feature"},
        glue = {"com.orangehrmlive.stepdefinitions"}
)
public class ManageMyInfoQualificationsTest {

}
