package in.reqres.runners.patch;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/reqres/patch/updateUser.feature"},
        glue = "in.reqres.stepdefinitions.patch",
        plugin = {"pretty", "html:target/cucumber-reports"}
)
//Comentario prueba
public class UpdateUserRunner {
}
