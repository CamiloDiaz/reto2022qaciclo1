package in.reqres.runners.delete;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        features = {"src/test/resources/features/reqres/delete/deleteUser.feature"},
        glue = "in.reqres.stepdefinitions.delete",
        plugin = {"pretty", "html:target/cucumber-reports"}
)

public class DeleteUserRunner {
}
