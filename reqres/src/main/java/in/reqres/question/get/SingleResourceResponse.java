package in.reqres.question.get;

import in.reqres.models.resource.Resource;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class SingleResourceResponse implements Question<Resource> {
    public static SingleResourceResponse singleResourceResponse() {
        return new SingleResourceResponse();
    }

    @Override
    public Resource answeredBy(Actor actor) {
        return SerenityRest.lastResponse().as(Resource.class);
    }
}
