package in.reqres.models.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonPropertyOrder({
        "id",
        "email",
        "first_name",
        "last_name",
        "avatar"
})
public class DataUser {
    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("id")
    private Integer id;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("email")
    private String email;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("first_name")
    private String firstName;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("last_name")
    private String lastName;

    @JsonInclude(Include.NON_DEFAULT)
    @JsonProperty("avatar")
    private String avatar;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
