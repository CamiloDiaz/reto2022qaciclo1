package in.reqres.util;

public enum PatchResources implements ReplaceStringRegex {
    UPDATE_USER_WITH_PATCH("/api/users/{id}");

    private final String value;

    PatchResources(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String replaceParam(String param) {
        return replaceString(value, param);
    }
}
