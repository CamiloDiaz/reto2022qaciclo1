package in.reqres.util;

public enum DeleteResources implements ReplaceStringRegex{
    DELETE_USER("/api/users/{id}");

    private final String value;

    DeleteResources(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String replaceParam(String param) {
        return replaceString(value,param);
    }
}
