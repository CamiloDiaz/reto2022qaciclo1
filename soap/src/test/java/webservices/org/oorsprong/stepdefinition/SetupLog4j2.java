package webservices.org.oorsprong.stepdefinition;

import org.apache.log4j.PropertyConfigurator;

import java.util.Locale;

import static com.google.common.base.StandardSystemProperty.USER_DIR;
import static webservices.org.oorsprong.util.Log4jValues.LOG4J_LINUX_PROPERTIES_FILE_PATH;
import static webservices.org.oorsprong.util.Log4jValues.LOG4J_PROPERTIES_FILE_PATH;

public class SetupLog4j2 {


    protected void setup()
    {
        String os = System.getProperty("os.name").toLowerCase(Locale.ROOT).substring(0,3);
        switch(os)
        {
            case "win":
                PropertyConfigurator.configure(USER_DIR.value() + LOG4J_PROPERTIES_FILE_PATH.getValue());
                break;

            case "lin":
                PropertyConfigurator.configure(USER_DIR.value() + LOG4J_LINUX_PROPERTIES_FILE_PATH.getValue());
                break;
        }
    }
}
